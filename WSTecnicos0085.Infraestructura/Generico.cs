﻿using Newtonsoft.Json.Linq;
using TCS.API.Entidades;
using WSTecnicos0085.Dominio.Interfaces;
using WSTecnicos0085.Dominio.Interfaces.Propiedades;
using WSTecnicos0085.Entidades;
using TCS.Comun.Extensiones;
using TCS.API.Entidades.Excepciones;
using TCS.Comun.Logs.Base.Handlers;

namespace WSTecnicos0085.Infraestructura
{
    public class Generico : IGenericoInfraestructura
    {
        private readonly IPropiedadesApi iPropiedadesApi;
        private readonly IGenericoRepositorio iGenericoRepositorio;
        public Generico(IPropiedadesApi iPropiedadesApi, IGenericoRepositorio iGenericoRepositorio)
        {
            this.iPropiedadesApi = iPropiedadesApi;
            this.iGenericoRepositorio = iGenericoRepositorio;
        }

        [Loggable]
        public void GenerarMesaje01(JObject entrada)
        {
            if (entrada.IsNull())
            {
                throw new CoreNegocioError(EConstantes.CodEntradaNull, EConstantes.DesEntradaNull, EConstantes.Recurso01, EConstantes.Componente, iPropiedadesApi.BackEndOpenShif());
            }
            JToken jTokenHeaderIn = entrada.SelectToken(EConstantes.headerIn);
            if (jTokenHeaderIn.IsNull())
            {
                throw new CoreNegocioError(EConstantes.CodHeaderNull, EConstantes.DesHeaderNull, EConstantes.Recurso01, EConstantes.Componente, iPropiedadesApi.BackEndOpenShif());
            }
            EHeader eHeader = jTokenHeaderIn.ToObject<EHeader>();
            if (string.IsNullOrEmpty(eHeader.Filler))
            {
                throw new CoreNegocioError(EConstantes.CodTopicoNull, EConstantes.DesTopicoNull, EConstantes.Recurso01, EConstantes.Componente, iPropiedadesApi.BackEndOpenShif());
            }
            JToken jTokenBodyIn = entrada.SelectToken(EConstantes.bodyIn);

            if (jTokenBodyIn.IsNull())
            {
                throw new CoreNegocioError(EConstantes.CodBodyNull, EConstantes.DesBodyNull, EConstantes.Recurso01, EConstantes.Componente, iPropiedadesApi.BackEndOpenShif());
            }
            iGenericoRepositorio.GenerarMensajeProducer01(eHeader.Filler, entrada.ToString());

        }
    }
}
