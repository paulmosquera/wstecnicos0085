﻿using Microsoft.AspNetCore.Mvc;
using System;
using TCS.API.Constantes;
using TCS.API.Entidades;
using TCS.API.Entidades.Excepciones;
using WSTecnicos0085.Dominio.Interfaces;
using WSTecnicos0085.Dominio.Interfaces.Propiedades;
using WSTecnicos0085.Entidades;
using Newtonsoft.Json.Linq;
using System.Net;
using TCS.Comun.Extensiones;
using TCS.Comun.Logs.Base.Handlers;

namespace WSTecnicos0085.API.Controllers
{


    [ApiVersion("1")]
    [Route(Controlador.RutaVersion)]
    [ApiController]
    public class GenericoController : Controller
    {

        private readonly IGenericoInfraestructura iGenerico;
        private readonly IPropiedadesApi iPropiedad;

        public GenericoController(IGenericoInfraestructura iGenerico, IPropiedadesApi iPropiedad)
        {
            this.iGenerico = iGenerico;
            this.iPropiedad = iPropiedad;
        }


        public IActionResult Get()
        {
            return Ok(new EEntrada<object>(new EHeader(), new object()));
        }

        [HttpPost]
        [Loggable]
        [Route("generarMensaje01")]
        public IActionResult GenerarMesaje01(JObject entrada)
        {
            EError error = new EError(EConstantes.Componente, EConstantes.Recurso01, iPropiedad.BackEndOpenShif());
            ERespuesta<object> salida = new ERespuesta<object>(new EHeader(), null, error);
            try
            {
                iGenerico.GenerarMesaje01(entrada);
                return Ok(salida);
            }
            catch (Exception excepcion)
            {
                return StatusCode(HttpStatusCode.BadRequest.ToInt(), excepcion.ProcesarError(salida));
            }
        }
    }
}
