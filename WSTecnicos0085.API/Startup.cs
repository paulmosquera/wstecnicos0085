﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TCS.Procesar.Recursos;

using WSTecnicos0085.Dominio.Interfaces;
using WSTecnicos0085.Repositorio;
using WSTecnicos0085.Infraestructura;
using WSTecnicos0085.Dominio.Interfaces.Propiedades;
using WSTecnicos0085.Repositorio.Propiedad;
using TCS.EventBus.Interfaces;
using TCS.EventBus;
using WSTecnicos0085.Repositorio.Configuraciones.Kafka;
using TCS.Comun.Centralizada.Interfaces;
using WSTecnicos0085.Repositorio.Configuraciones.Centralizada;
using TCS.Comun.Centralizada;
using TCS.Comun.Logs.Implementacion;

namespace WSTecnicos0085.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            #region INYECCION DE DEPENDENCIA


            services.AddTransient<IGenericoRepositorio, RepositorioGenerico>();
            services.AddTransient<IGenericoInfraestructura, Generico>();


            services.AddSingleton<IPropiedadesApi, PropiedadesApi>();           
           
            services.AddSingleton<IConfiguradorEventBus, ConfiguracionKafka>();
            services.AddSingleton<IEventBusStreaming, EventBusStreamingKafka>();

            services.AddSingleton<IConfiguradorCentralizada, ConfiguradorCentralizada>();
            services.AddSingleton<IConfiguracionCentralizada, ConfiguracionCentralizada>();


            #endregion INYECCION DE DEPENDENCIA FIN

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            #region MANEJO DE VERSIONES DE API
            services.AddApiVersioning(options => options.UseApiBehavior = true);
            services.AddApiVersioning(options => options.AssumeDefaultVersionWhenUnspecified = true);
            #endregion MANEJO DE VERSIONES DE API FIN

            ConfiguradorLog.InicializarDefault();
            //Creación de política para Cors Domain
            services.AddCors(options => options.AddPolicy("AllowAll", p => p.AllowAnyOrigin()
                                                                   .AllowAnyMethod()
                                                                    .AllowAnyHeader()));

            string configuracionWs = Configuration.GetValue<string>("Configuracion");
            Configuraciones.Inicializar(configuracionWs);
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //Uso de la política de Cors Domain
            app.UseCors("AllowAll");
            app.UseMvc();

        }
    }
}
