﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace WSTecnicos0085.API
{
    public static class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        // ANTERIOR
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
