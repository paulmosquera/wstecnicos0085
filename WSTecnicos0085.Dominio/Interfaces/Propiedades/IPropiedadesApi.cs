﻿
namespace WSTecnicos0085.Dominio.Interfaces.Propiedades
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPropiedadesApi
    {     

        /// <summary>
        /// Retorna el valor desde las configuraciones de la API
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        string ConsultarApi(string propiedad);
                

        string ConsultarCatalogo(string propiedad);

        /// <summary>
        /// Método que permite consultar un valor desde configuración centralizada
        /// </summary>
        /// <param name="propiedad"></param>
        /// <returns></returns>
        string ConsultarCentralizada(string propiedad);

        #region Propiedades especificas del ensamblado
        string BackEndOpenShif();
        string CodFatal();
        string TipoFatal();
        string CodCampoVacio();
        string DesCampoVacio();
        string Recurso01();
        string Componente01();
        string InicialTopico();
        string ServidorKafka0();
        string ServidorKafka1();
        string TimeOutKafka();


        #endregion Propiedades especificas del ensamblado fin
    }
}
