﻿using Newtonsoft.Json.Linq;

namespace WSTecnicos0085.Dominio.Interfaces
{
    public interface IGenericoInfraestructura
    {
        void GenerarMesaje01(JObject entrada);
    }
}
