using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using TCS.API.Entidades;
using TCS.API.Entidades.Excepciones;
using WSTecnicos0085.Dominio.Interfaces;
using WSTecnicos0085.Dominio.Interfaces.Propiedades;
using WSTecnicos0085.Entidades;

using WSTecnicos0085.Infraestructura;

namespace WSTecnicos0085.Test
{
    public class Tests
    {
        Mock<IPropiedadesApi> mockIPropiedad;
        Mock<IGenericoRepositorio> mockRepositorio;
        JObject entradaJOject;

        [SetUp]
        public void Setup()
        {
            string jsonText = @"{
		                        'HeaderIn': {
			                        'dispositivo': '01010101010101010101010101010101',
			                        'empresa': '0010',
			                        'canal': '02',
			                        'medio': '020002',
			                        'aplicacion': '00647',
			                        'agencia': '',
			                        'tipoTransaccion': '204000301',
			                        'geolocalizacion': '',
			                        'usuario': 'USINTERT',
			                        'unicidad': 'UZhDOa2GXtkAK2/QovFagnnS+nQSGAF/SKyTIIv1axc=',
			                        'guid': '3F2504E04F8911D39A0C0305E82C3302',
			                        'fechaHora': '201702081505651262',
			                        'filler': 'OFERTA_GANADA',
			                        'idioma': 'es-EC',
			                        'sesion': 'ASDASD77R01YH',
			                        'ip': '10.0.94.126',
			                        'idCliente': '1718272600',
			                        'tipoIdCliente': '0001'
		                        },
		                        'BodyIn': {
			                        'Cliente': {
				                        'cif': '0000000200042240',
				                        'identificacion':'1717197920',
				                        'tipoIdentificacion':'0001'
			                        },
			                        'Oferta':	{
				                        'id':'12345678',
				                        'descripcion':'PRECISO PREAPROBADO JUAN PEREZ',
				                        'codigoProducto':'21918-58634-21977',
				                        'garantia':false,
				                        'monto':'5500',
				                        'tasa':'16.80',
				                        'plazo':'36',
				                        'numeroTransaccion':'',
				                        'canal':'02',
				                        'medio':'020002',
				                        'aplicacion':'00647',
				                        'fechaVigencia':'20191130',
				                        'tipo':'BP_Oferta',
				                        'codigoMotivoPerdida': '',
				                        'codigoEstadoOferta':'5'
			                        }
		                        }
                        } ";
            entradaJOject = JObject.Parse(jsonText);
        }

        [Test]
        public void IngresoDatosGenerico_ExisteInformacionCompleta_RetornoPositivo()
        {
            mockIPropiedad = new Mock<IPropiedadesApi>();
            mockRepositorio = new Mock<IGenericoRepositorio>();

            JToken jTokenHeaderIn = entradaJOject.SelectToken(EConstantes.headerIn);
            EHeader eHeader = jTokenHeaderIn.ToObject<EHeader>();
            mockRepositorio.Setup(x => x.GenerarMensajeProducer01(eHeader.Filler, entradaJOject.ToString()));

            IGenericoInfraestructura ponerPilaKafkaServicio = new Generico(mockIPropiedad.Object, mockRepositorio.Object);
            Assert.DoesNotThrow(() => ponerPilaKafkaServicio.GenerarMesaje01(entradaJOject));
        }

        [Test]
        public void IngresoDatosGenerico_EntradaNUll_RetornoNegativo()
        {
            mockIPropiedad = new Mock<IPropiedadesApi>();
            mockRepositorio = new Mock<IGenericoRepositorio>();

            JToken jTokenHeaderIn = entradaJOject.SelectToken(EConstantes.headerIn);
            EHeader eHeader = jTokenHeaderIn.ToObject<EHeader>();
            mockRepositorio.Setup(x => x.GenerarMensajeProducer01(eHeader.Filler, entradaJOject.ToString()));
            entradaJOject = null;
            IGenericoInfraestructura ponerPilaKafkaServicio = new Generico(mockIPropiedad.Object, mockRepositorio.Object);
            Assert.Throws<CoreNegocioError>(() => ponerPilaKafkaServicio.GenerarMesaje01(entradaJOject));
        }

        [Test]
        public void IngresoDatosGenerico_HeaderInNUll_RetornoNegativo()
        {
            mockIPropiedad = new Mock<IPropiedadesApi>();
            mockRepositorio = new Mock<IGenericoRepositorio>();

            JToken jTokenHeaderIn = entradaJOject.SelectToken(EConstantes.headerIn);
            EHeader eHeader = jTokenHeaderIn.ToObject<EHeader>();
            mockRepositorio.Setup(x => x.GenerarMensajeProducer01(eHeader.Filler, entradaJOject.ToString()));
            entradaJOject.Property(EConstantes.headerIn).Remove();
            IGenericoInfraestructura ponerPilaKafkaServicio = new Generico(mockIPropiedad.Object, mockRepositorio.Object);
            Assert.Throws<CoreNegocioError>(() => ponerPilaKafkaServicio.GenerarMesaje01(entradaJOject));
        }

        [Test]
        public void IngresoDatosGenerico_BodyInNUll_RetornoNegativo()
        {
            mockIPropiedad = new Mock<IPropiedadesApi>();
            mockRepositorio = new Mock<IGenericoRepositorio>();

            JToken jTokenHeaderIn = entradaJOject.SelectToken(EConstantes.headerIn);
            EHeader eHeader = jTokenHeaderIn.ToObject<EHeader>();
            mockRepositorio.Setup(x => x.GenerarMensajeProducer01(eHeader.Filler, entradaJOject.ToString()));
            entradaJOject.Property(EConstantes.bodyIn).Remove();
            IGenericoInfraestructura ponerPilaKafkaServicio = new Generico(mockIPropiedad.Object, mockRepositorio.Object);
            Assert.Throws<CoreNegocioError>(() => ponerPilaKafkaServicio.GenerarMesaje01(entradaJOject));
        }


    }
}