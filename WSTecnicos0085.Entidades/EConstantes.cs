﻿
namespace WSTecnicos0085.Entidades
{
    /// <summary>
    /// Constantes de API
    /// </summary>
    public static class EConstantes
    {
        /// <summary>
        /// Propiedad de obtiene el nombre del componente.
        /// </summary>
        public const string Componente = "WSTecnicos0085";

        /// <summary>
        /// Propiedad que obtiene nombre de método.
        /// </summary>
        public const string Recurso01 = "GenerarMesaje01";
        /// <summary>
        /// Propiedad que obtiene nombre de método.
        /// </summary>
        public const string Recurso02 = "GenerarMensajeProducer01";


        
        /// <summary>
        /// Codigo que representa el campo vacio.
        /// </summary>
        public const string CodCampoVacio = "0001";
        /// <summary>
        /// Descripcion que representa el campo vacio.
        /// </summary>
        public const string DesCampoVacio = "UNO O VARIOS CAMPOS DE ENTRADA ESTAN VACIOS";

        /// <summary>
        /// Codigo que representa la entrada vacio.
        /// </summary>
        public const string CodEntradaNull = "0002";
        /// <summary>
        /// Descripcion que representa la entrada vacio.
        /// </summary>
        public const string DesEntradaNull = "LA ENTRADA ES NULA O NO EXISTE";
        /// <summary>
        /// Codigo que representa el header vacio.
        /// </summary>
        public const string CodHeaderNull = "0003";
        /// <summary>
        /// Descripcion que representa el header vacio.
        /// </summary>
        public const string DesHeaderNull = "LA CABECERA ES NULA O NO EXISTE";
        /// <summary>
        /// Codigo que representa el topico vacio.
        /// </summary>
        public const string CodTopicoNull = "0004";
        /// <summary>
        /// Descripcion que representa el topico vacio.
        /// </summary>
        public const string DesTopicoNull = "EL TOPICO ES NULA O NO EXISTE";
        /// <summary>
        /// Codigo que representa el body vacio.
        /// </summary>
        public const string CodBodyNull = "0005";
        /// <summary>
        /// Descripcion que representa el body vacio.
        /// </summary>
        public const string DesBodyNull = "EL CUERPO DEL SERVICIO ESTA NULO O NO EXISTE";

        /// <summary>
        /// Header
        /// </summary>
        public const string headerIn = "HeaderIn";
        /// <summary>
        /// Body
        /// </summary>
        public const string bodyIn = "BodyIn";

        


    }
}
