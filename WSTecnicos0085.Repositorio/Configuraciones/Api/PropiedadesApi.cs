﻿

using TCS.Procesar.Recursos.ConfiguracionCentralizada;
using TCS.Procesar.Recursos.Propiedades;
using WSTecnicos0085.Dominio.Interfaces.Propiedades;

namespace WSTecnicos0085.Repositorio.Propiedad
{
    /// <summary>
    /// Clase que permite inicializar la configuracion de archivos de propiedades
    /// </summary>
    public class PropiedadesApi : IPropiedadesApi
    {
        private readonly Propiedades configuracionCatalogos;        
        private readonly Propiedades configuracionApi;

        public PropiedadesApi()
        {
            configuracionCatalogos = new Propiedades("/opt/app-root/configs/catalogo/catalogos.json");            
            configuracionApi = new Propiedades("/opt/app-root/configs/servicio/wstecnicos0085.json");
        }



        /// <summary>
        /// Permite consultar información específica de la implementación
        /// </summary>
        /// <param name="propiedad">nombre de propiedad con la cual se consultará su valor</param>
        /// <returns></returns>
        public string ConsultarApi(string propiedad)
        {
            return configuracionApi.Get(propiedad);
        }

        /// <summary>
        /// Permite consultar información del catálogo de aplicaciones
        /// </summary>
        /// <param name="propiedad">nombre de propiedad con la cual se consultará su valor</param>
        /// <returns></returns>
        public string ConsultarCatalogo(string propiedad)
        {
            return configuracionCatalogos.Get(propiedad);
        }

        /// <summary>
        /// Permite consultar información desde configuración centralizada
        /// </summary>
        /// <param name="propiedad">nombre de propiedad con la cual se consultará su valor</param>
        /// <returns></returns>
        public string ConsultarCentralizada(string propiedad)
        {
            return CCentralizada.Obtener(propiedad);
        }
        public string BackEndOpenShif()
        {
            return ConsultarCatalogo("BACKEND_OPENSHIFT");
        }

        public string CodCampoVacio()
        {
            return ConsultarApi("COD_CAMPOS_VACIOS");
        }

        public string DesCampoVacio()
        {
            return ConsultarApi("DES_CAMPOS_VACIOS");
        }

        public string Recurso01()
        {
            return ConsultarApi("RECURSO_01");
        }

        public string Componente01()
        {
            return ConsultarApi("COMPONENTE_01");
        }

        public string InicialTopico()
        {
            return ConsultarApi("INICIAL_TOPICO");
        }

        public string ServidorKafka0()
        {
            return ConsultarApi("SERVIDOR_KAFKA0");
        }

        public string ServidorKafka1()
        {
            return ConsultarApi("SERVIDOR_KAFKA1");
        }

        public string CodFatal()
        {
            return ConsultarApi("COD_FATAL");
        }

        public string TipoFatal()
        {
            return ConsultarApi("TIPO_FATAL");
        }

        public string TimeOutKafka()
        {
            return ConsultarApi("TIME_OUT_KAFKA");
        }
    }
}
