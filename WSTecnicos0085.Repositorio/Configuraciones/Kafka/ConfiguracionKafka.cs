﻿using TCS.EventBus.Interfaces;
using WSTecnicos0085.Dominio.Interfaces.Propiedades;
using TCS.Comun.Centralizada.Interfaces;
using System;

namespace WSTecnicos0085.Repositorio.Configuraciones.Kafka
{
    public class ConfiguracionKafka : IConfiguradorEventBus
    {
        private readonly IPropiedadesApi iPropiedadesApi;
        private readonly IConfiguracionCentralizada iConfiguracionCentralizada;
        public ConfiguracionKafka(IPropiedadesApi iPropiedadesApi, IConfiguracionCentralizada iConfiguracionCentralizada)
        {
            this.iPropiedadesApi = iPropiedadesApi;
            this.iConfiguracionCentralizada = iConfiguracionCentralizada;
        }

        public string IdGrupo()
        {
            return null;
        }
        public string Servidores()
        {
            string servidor01 = iConfiguracionCentralizada.ConsultarTag(iPropiedadesApi.ServidorKafka0());
            string servidor02 = iConfiguracionCentralizada.ConsultarTag(iPropiedadesApi.ServidorKafka1());
            return $"{servidor01},{servidor02}";
        }

        public int TimeOut()
        { 
            return Int32.Parse(iPropiedadesApi.TimeOutKafka());
        }

        public string Topico()
        {
            return null;
        }
    }
}
