﻿using System;
using TCS.Comun.Centralizada.Interfaces;
using TCS.Comun.Centralizada.Entidades;
using TCS.Procesar.Recursos.Propiedades;
using TCS.Comun.Extensiones;
using WSTecnicos0085.Dominio.Interfaces.Propiedades;

namespace WSTecnicos0085.Repositorio.Configuraciones.Centralizada
{
    public class ConfiguradorCentralizada : IConfiguradorCentralizada
    {
        private readonly Propiedades propiedadesCentralizada;
        private readonly IPropiedadesApi iPropiedadesApi;

        public ConfiguradorCentralizada(IPropiedadesApi iPropiedadesApi)
        {            
            propiedadesCentralizada = new Propiedades(Constantes.RutaConfiguracion);
            this.iPropiedadesApi = iPropiedadesApi;
        }

        public bool AutoCarga()
        {
            return iPropiedadesApi.ConsultarApi(Constantes.TagAutoCarga).ToBool();
        }

        public string CodigoAplicacion()
        {
            return iPropiedadesApi.ConsultarApi(Constantes.TagCodigoAplicacion);
        }

        public int Reintentos()
        {
            return iPropiedadesApi.ConsultarApi(Constantes.TagReintentos).ToInt();
        }





        public int TimeOut()
        {
            return propiedadesCentralizada.Get(Constantes.TagTimeOut).ToInt();
        }

        public string UrlWsCodigo()
        {
            return propiedadesCentralizada.Get(Constantes.TagUrlWsCodigo);
        }

        public string UrlWsConfiguracion()
        {
            return propiedadesCentralizada.Get(Constantes.TagUrlWsConfiguracion);
        }

        public string DirectorioLogs()
        {
            return null;
        }

        public bool SegundoPlano()
        {
            throw new NotImplementedException();
        }
    }
}
