﻿using System;
using TCS.API.Entidades.Excepciones;
using TCS.EventBus.Interfaces;
using WSTecnicos0085.Dominio.Interfaces;
using WSTecnicos0085.Dominio.Interfaces.Propiedades;
using WSTecnicos0085.Entidades;

namespace WSTecnicos0085.Repositorio
{
    public class RepositorioGenerico : IGenericoRepositorio
    {
        private readonly IPropiedadesApi iPropiedadesApi;
        private readonly IEventBusStreaming iEventBus;
        public RepositorioGenerico(IPropiedadesApi iPropiedadesApi, IEventBusStreaming iEventBus)
        {
            this.iPropiedadesApi = iPropiedadesApi;
            this.iEventBus = iEventBus;
        }

        public void GenerarMensajeProducer01(string topico, string mensaje)
        {
            try
            {
                topico = $"{iPropiedadesApi.InicialTopico()}{topico}";
                iEventBus.Producer(topico, mensaje);
            }
            catch (Exception excepcion)
            {
                throw new CoreExcepcion(EConstantes.Componente, EConstantes.Recurso02, "00255", excepcion);
            }
        }
    }
}
